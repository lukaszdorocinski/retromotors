import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Notice } from '../GlobalDataModel/notice';
import { GlobalService } from '../services/global.service';
import { NoticeService } from '../services/notice.service';
import { NgxGalleryOptions, NgxGalleryImage } from 'ngx-gallery';
import { UserService } from '../services/user.service';
import { SharedService } from '../services/shared.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.css']
})
export class NoticeComponent implements OnInit {

  notice: Notice;
  username = '';
  hide = true;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private globalService: GlobalService,
    private noticeService: NoticeService,
    private userService: UserService,
    private cookieService: CookieService,
    private location: Location,
  ) { }

  ngOnInit() {
    window.scrollTo(0, 320);
    this.notice = this.globalService.booleanParse(this.route.snapshot.data['notice'][0]);
    // console.log(this.notice);

    this.route
      .queryParams
      .subscribe(params => {
        if ((params['userAccept']) === 'true') { this.hide = false; }
      });

    this.setGallery();
    this.addDisplayCounter();
    this.setUsername();

    // ustawienie parametru hide
    this.patchValue();
  }

  setUsername() {
    this.userService.getUserById(this.notice.UserID).subscribe(
      data => {
        if (data[0].nick_name !== '') {
          this.username = data[0].nick_name;
        } else {
          this.username = data[0].email;
        }
      });
  }

  patchValue() {
    const token = this.cookieService.get('token');
    this.userService.getUserByToken(this.cookieService.get('token')).subscribe(
      data => {
        if (data) {
          if (data[0].id.toString() === this.notice.UserID) {
            this.hide = false;
          }
        }
      });
  }

  setGallery() {
    this.galleryOptions = [
      { 'previewCloseOnClick': true, 'previewCloseOnEsc': true },
      { 'breakpoint': 1920, 'width': '100%', 'height': '1024px', 'thumbnailsColumns': 4 },
      { 'breakpoint': 1600, 'width': '100%', 'height': '860px', 'thumbnailsColumns': 4 },
      { 'breakpoint': 1480, 'width': '100%', 'height': '800px', 'thumbnailsColumns': 4 },
      { 'breakpoint': 1280, 'width': '100%', 'height': '640px', 'thumbnailsColumns': 4 },
      { 'breakpoint': 1024, 'width': '100%', 'height': '550px', 'thumbnailsColumns': 4 },
      { 'breakpoint': 860, 'width': '100%', 'height': '400px', 'thumbnailsColumns': 3 },
      { 'breakpoint': 640, 'width': '100%', 'height': '300px', 'thumbnailsColumns': 3 },
      { 'breakpoint': 480, 'width': '100%', 'height': '200px', 'thumbnailsColumns': 2 },
      // {
      //   width: '800px',
      //   height: '600px',
      //   thumbnailsColumns: 4
      // }
    ];

    this.galleryImages = [
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image1,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image1,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image1,
      },
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image2,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image2,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image2,
      },
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image3,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image3,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image3,
      },
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image4,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image4,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image4,
      },
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image5,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image5,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image5,
      },
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image6,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image6,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image6,
      },
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image7,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image7,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image7,
      },
      {
        small: 'http://api.retromotors.pl/images/' + this.notice.Image8,
        medium: 'http://api.retromotors.pl/images/' + this.notice.Image8,
        big: 'http://api.retromotors.pl/images/' + this.notice.Image8,
      },
    ];
  }

  setUserAccept() {
    this.notice.UserAccept = true;
    const select = { table: 'notice', bind: this.notice, where: { Guid: this.notice.Guid } };
    this.noticeService.updateNotice(select)
      .subscribe(
        data => {
          if (data === true) {
            this.router.navigate(['/UserPanel/']);
          }
        });
  }

  goToEditNotice() {
    this.router.navigate(['/NoticeEdit/' + this.notice.Guid]);
  }

  delNotice() {
    this.notice.IsDeleted = true;
    const select = { table: 'notice', bind: this.notice, where: { Guid: this.notice.Guid } };
    this.noticeService.updateNotice(select)
      .subscribe(
        data => {
          if (data === true) {
            this.router.navigate(['/UserPanel/']);
          }
        });
  }

  addDisplayCounter() {
    this.notice.DisplayCounter++;
    const select = { table: 'notice', bind: this.notice, where: { Guid: this.notice.Guid } };
    this.noticeService.updateNotice(select)
      .subscribe(
        data => {
          if (data === true) {
            // console.log(this.notice.DisplayCounter);
          }
        });
  }

  back() {
    this.location.back();
  }

}
