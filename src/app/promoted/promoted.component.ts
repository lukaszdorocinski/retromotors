import { Component, OnInit, Input } from '@angular/core';
import { Notice } from '../GlobalDataModel/notice';
import { NoticeService } from '../services/notice.service';

@Component({
  selector: 'app-promoted',
  templateUrl: './promoted.component.html',
  styleUrls: ['./promoted.component.css']
})
export class PromotedComponent implements OnInit {
  @Input() bigSize: boolean;
  noticeList: Notice[];
  bigNoticeList: Notice[];
  smallNoticeList: Notice[];

  constructor(
    private noticeService: NoticeService,
  ) { }

  ngOnInit() {
    console.log(this.bigSize);
    this.takeAllNotice();
  }

  takeAllNotice() {
    this.noticeService.getAllActiveNotice().subscribe(
      data => {
        this.noticeList = data;
        this.takeBigNotice();
        this.takeSmallNotice();
      });
  }

  takeBigNotice() {
    this.bigNoticeList = this.noticeList.reverse().slice(0, 3);
    // console.log(this.bigNoticeList);
  }

  takeSmallNotice() {
    this.smallNoticeList = this.noticeList.slice(3, 9);
    // console.log(this.smallNoticeList);
  }
}
