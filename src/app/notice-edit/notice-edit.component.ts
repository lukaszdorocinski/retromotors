import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Notice } from '../GlobalDataModel/notice';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { NoticeService } from '../services/notice.service';
import { SharedService } from '../services/shared.service';
import { MatStepper } from '@angular/material';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-notice-edit',
  templateUrl: './notice-edit.component.html',
  styleUrls: ['./notice-edit.component.css']
})
export class NoticeEditComponent implements OnInit {

  notice: Notice;

  options: FancyImageUploaderOptions = {
    thumbnailHeight: 250,
    thumbnailWidth: 250,
    authToken: btoa(this.globalService.username + ':' + this.globalService.password),
    authTokenPrefix: 'Basic',
    uploadUrl: 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/upload',
    allowedImageTypes: ['image/png', 'image/jpeg'],
    maxImageSize: 5,
  };

  validationParametr = false; // parametr for check validation fields

  date = new Date();
  // do zaciągnięcia z bazy i cache
  marki: any[];
  modele = [];
  modeleTemp: any[];
  years: any[];
  typyNadwozia: any[];
  rodzajPaliwa: any[];
  style: any[];
  kierownice: any[];
  stany: any[];
  skrzynie: any[];
  napedy: any[];
  kraje: any[];
  liczbaDrzwi: any[];
  wojewodztwa: any[];
  kolory: any[];

  isSell = false;
  isRent = false;
  isMoreData = false;
  isLinear = false;

  modelForm: FormGroup;
  firstFormGroup: any;

  formErrors = {
    NoticeTitle: '',
    Mark: '',
    Model: '',
  };

  private validationMessages = {
    NoticeTitle: {
      required: 'Należy uzupełnić opis'
    },
    Mark: {
      required: 'Należy wybrać markę samochodu'
    },
    Model: {
      required: 'Należy wybrać model samochodu'
    }
  };

  constructor(
    private formBuilder: FormBuilder,
    private noticeService: NoticeService,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private router: Router,
    private globalService: GlobalService,
  ) { }

  ngOnInit() {
    this.notice = this.globalService.booleanParse(this.route.snapshot.data['notice'][0]);
    this.patchArray();

    this.modelForm = this.formBuilder.group({
      Id: this.notice.Id,
      NoticeTitle: [this.notice.NoticeTitle, Validators.required],
      SellingPrice: this.notice.SellingPrice,
      IsSelling: this.notice.IsSelling,
      RentalPrice: this.notice.RentalPrice,
      IsRental: this.notice.IsRental,
      Mark: [this.notice.Mark, Validators.required],
      Model: [this.notice.Model, Validators.required],
      Style: this.notice.Style,
      ProductionYear: this.notice.ProductionYear,
      EngineDisplacement: this.notice.EngineDisplacement,
      HorsePower: this.notice.HorsePower,
      Mileage: this.notice.Mileage,
      Colour: this.notice.Colour,
      VinNo: this.notice.VinNo,
      Fuel: this.notice.Fuel,
      BodyType: this.notice.BodyType,
      SteeringWheel: this.notice.SteeringWheel,
      Condition: this.notice.Condition,
      Transmission: this.notice.Transmission,
      Drive: this.notice.Drive,
      Country: this.notice.Country,
      RegisteredIn: this.notice.RegisteredIn,
      DoorsNo: Number(this.notice.DoorsNo),
      SeatsNo: Number(this.notice.SeatsNo),
      ElectricMirrors: this.notice.ElectricMirrors,
      ElectricWindow: this.notice.ElectricWindow,
      AirConditioning: this.notice.AirConditioning,
      CruiseControl: this.notice.CruiseControl,
      LeatherSeats: this.notice.LeatherSeats,
      HeatedSeats: this.notice.HeatedSeats,
      SunRoof: this.notice.SunRoof,
      Description: this.notice.Description,
      Photos: this.notice.Photos,
      City: this.notice.City,
      PhoneNo: this.notice.PhoneNo,
      Voivodeship: this.notice.Voivodeship,
      UserID: this.notice.UserID,
      Email: this.notice.Email,
      Guid: this.notice.Guid,
      Image1: this.notice.Image1,
      Image2: this.notice.Image2,
      Image3: this.notice.Image3,
      Image4: this.notice.Image4,
      Image5: this.notice.Image5,
      Image6: this.notice.Image6,
      Image7: this.notice.Image7,
      Image8: this.notice.Image8,
      UserAccept: false,
      AdminAccept: false,
      IsPromoted: false,
      StartDate: this.date,
      EndDate: '',
      IsArchival: false,
      IsDeleted: false,
      DisplayCounter: 0,
    });

    this.modelForm.valueChanges.subscribe((value: FormGroup) => {
      this.onControlValueChanged();
    });
    this.onControlValueChanged();

    this.patchValue();

  }

  patchArray() {
    this.marki = this.globalService.marki;
    this.modeleTemp = this.globalService.modeleTemp;
    this.years = this.globalService.years;
    this.typyNadwozia = this.globalService.typyNadwozia;
    this.rodzajPaliwa = this.globalService.rodzajPaliwa;
    this.style = this.globalService.style;
    this.kierownice = this.globalService.kierownice;
    this.stany = this.globalService.stany;
    this.skrzynie = this.globalService.skrzynie;
    this.napedy = this.globalService.napedy;
    this.kraje = this.globalService.kraje;
    this.liczbaDrzwi = this.globalService.liczbaDrzwi;
    this.wojewodztwa = this.globalService.wojewodztwa;
    this.kolory = this.globalService.kolory;
  }

  patchValue() {
    if (this.notice.IsSelling === true) {
      this.isSell = true;
    }
    if (this.notice.IsRental === true) {
      this.isRent = true;
    }
    this.isMoreData = true;
    this.setModel(this.notice.Mark);
  }

  // Validation method
  onControlValueChanged(): void {
    const form = this.modelForm;
    // tslint:disable:forin
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (this.validationParametr === false) {
        if (control && control.dirty && !control.valid) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      } else {
        if (control) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      }
    }
  }

  checkValidation(stepper: MatStepper): void {
    stepper.selectedIndex = 0;
    this.validationParametr = true;
    const form = this.modelForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }

  toogleSell() {
    this.isSell = !this.isSell;
  }

  toogleRent() {
    this.isRent = !this.isRent;
  }

  toogleMoreData() {
    this.isMoreData = !this.isMoreData;
  }

  setModel(marka: string) {
    this.modele = [{ value: 'Wszystkie', viewValue: 'Wszystkie' }];
    for (const model of this.modeleTemp) {
      if (model.value === marka) {
        this.modele.push(model);
      }
    }
  }

  onUpload(file: UploadedFile, image: string) {
    const splitted = file.response.split('"');
    this.modelForm.controls[image].setValue(splitted[9]);
  }

  onSubmit() {
    const select = { table: 'notice', bind: this.modelForm.value, where: { Guid: this.modelForm.value.Guid } };
    this.noticeService.updateNotice(select)
      .subscribe(
        data => {
          if (data === true) {
            this.router.navigate(['/Ogloszenie/' + this.modelForm.value.Guid], { queryParams: { userAccept: 'true' } });
          }
        });
  }
  // end
}
