import { Location } from '@angular/common';

export class Common {
    constructor(
        private location?: Location
    ) { }

    goBack() {
         this.location.back();
    } 

    //showErrorOrGoBack(errorMessage): void {
    //    if (errorMessage !== "") {
    //        alert(errorMessage);
    //        return;
    //    } else {
    //        return this.goBack();
    //    }
    //}

    handleError(error: any, message?: any): Promise<string> {
        let errorDetails: string = error.message !== "" ? error.message : "";
        if (error !== "Error") {
            message += error;
        }
        else if (typeof error.message != "undefined" && error.message !== "") {
            message += errorDetails;
        }
        else {
            message = "Wystąpił nieznany błąd";
        }

        return message;
    }

    //private ObservablehandleError(error: Response | any) {
    //    let errMsg: string;

    //    if (error instanceof Response) {
    //        const body = error.json() || '';
    //        const err = body.error || JSON.stringify(body);
    //        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    //    }
    //    else {
    //        errMsg = error.message ? error.message : error.toString();
    //    }
    //    return Observable.throw(errMsg);
    //}
}