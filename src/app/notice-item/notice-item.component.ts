import { Component, OnInit, Input } from '@angular/core';
import { Notice } from '../GlobalDataModel/notice';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notice-item',
  templateUrl: './notice-item.component.html',
  styleUrls: ['./notice-item.component.css']
})
export class NoticeItemComponent implements OnInit {
  @Input() item: Notice;

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    // console.log(this.item);
  }

  goToPreview(guid: string) {
    this.router.navigate(['Ogloszenie/' + guid]);
  }
}
