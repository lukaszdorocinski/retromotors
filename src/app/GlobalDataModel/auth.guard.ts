import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private cookieService: CookieService,
        private authService: AuthenticationService,
        private router: Router
    ) { }

    // canActivate() {
    //     if (this.sharedService.globalUser.email !== '') {
    //         return true;
    //     }
    //     this.router.navigate(['/Login']);
    //     return false;
    // }
    // canActivate() {
    //     // if (localStorage.getItem('currentUser')) {
    //     //     // logged in so return true
    //     //     return true;
    //     // }

    //     // // not logged in so redirect to login page
    //     // this.router.navigate(['/login']);
    //     // return false;

    //     const userData = JSON.parse(localStorage.getItem('currentUser'));
    //     if (userData) {
    //         return true;
    //     }
    //     this.router.navigate(['/Login']);
    //     return false;
    // }

    canActivate() {
        if (this.authService.isLoggedIn) {
            // sprawdzić czy jest user o takim tokenie
            return true;
        }
        this.router.navigate(['/Logowanie']);
        return false;
    }
}
