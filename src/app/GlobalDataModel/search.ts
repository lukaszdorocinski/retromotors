export class Search {
    query: string;
    Voivodeship: string;
    marka: string;
    model:  string;
    rokProdukcjiOd: number;
    rokProdukcjiDo: number;
    cenaOd: number;
    cenaDo: number;
    typNadwozia:  string;
    paliwo:  string;
    kolor: string;
    styl: string;
    isSelling: boolean;
    isRental: boolean;
}
