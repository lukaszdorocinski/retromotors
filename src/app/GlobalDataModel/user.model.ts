export class User {
    id: number;
    email: string;
    password: string;
    salt: string;
    hash_alg: string;
    registry_date: Date;
    last_login: Date;
    nick_name: string;
    session_hash: string;
}
