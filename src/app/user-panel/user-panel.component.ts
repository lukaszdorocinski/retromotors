import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from '../services/shared.service';
import { Notice } from '../GlobalDataModel/notice';
import { NoticeService } from '../services/notice.service';
import { AuthenticationService } from '../services/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserPanelComponent implements OnInit {

  notices: Notice[];
  username = '';

  constructor(
    private noticeService: NoticeService,
    private authService: AuthenticationService,
    private cookieService: CookieService,
    private userService: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    if (!this.authService.isLoggedIn) {
      this.authService.setLoalStorage();
    }
    const userData = JSON.parse(localStorage.getItem('currentUser'));
    if (userData.nickname !== null) {
      this.username = userData.nickname;
    } else { this.username = userData.username; }

    this.getNotice();

  }

  getNotice() {
    const token = this.cookieService.get('token');
    this.userService.getUserByToken(token).subscribe(
      data => {
        if (data) {
          this.noticeService.getNoticeByUserID(data[0].id).subscribe(
            x => this.notices = x);
        }
      });
  }

  goToDetails() {
    this.router.navigate(['/UserDetails/', 1]);
  }

}
