import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from '../services/shared.service';
import { User } from '../GlobalDataModel/user.model';
import { FormControl } from '@angular/forms/src/model';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { NoticeService } from '../services/notice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatStepper } from '@angular/material';
import { GlobalService } from '../services/global.service';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-notice-add',
  templateUrl: './notice-add.component.html',
  styleUrls: ['./notice-add.component.css']
})
export class NoticeAddComponent implements OnInit {

  options: FancyImageUploaderOptions = {
    thumbnailHeight: 250,
    thumbnailWidth: 250,
    authToken: btoa(this.globalService.username + ':' + this.globalService.password),
    authTokenPrefix: 'Basic',
    uploadUrl: 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/upload',
    allowedImageTypes: ['image/png', 'image/jpeg'],
    maxImageSize: 5,
  };

  validationParametr = false; // parametr for check validation fields

  date = new Date();

  marki: any[];
  modele = [];
  modeleTemp: any[];
  years: any[];
  typyNadwozia: any[];
  rodzajPaliwa: any[];
  style: any[];
  kierownice: any[];
  stany: any[];
  skrzynie: any[];
  napedy: any[];
  kraje: any[];
  liczbaDrzwi: any[];
  wojewodztwa: any[];
  kolory: any[];

  user: User;
  email = 'test email';

  isSell = false;
  isRent = false;
  isMoreData = false;

  isLinear = false;
  modelForm: FormGroup;
  firstFormGroup: any;

  formErrors = {
    NoticeTitle: '',
    Mark: '',
    Model: '',
  };

  private validationMessages = {
    NoticeTitle: {
      required: 'Należy uzupełnić opis'
    },
    Mark: {
      required: 'Należy wybrać markę samochodu'
    },
    Model: {
      required: 'Należy wybrać model samochodu'
    }
  };

  constructor(
    private formBuilder: FormBuilder,
    private noticeService: NoticeService,
    private cookieService: CookieService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private globalService: GlobalService,
  ) { }

  ngOnInit() {

    this.modelForm = this.formBuilder.group({
      Id: '',
      NoticeTitle: ['', Validators.required],
      SellingPrice: '',
      IsSelling: false,
      RentalPrice: '',
      IsRental: false,
      Mark: ['', Validators.required],
      Model: ['', Validators.required],
      Style: '',
      ProductionYear: '',
      EngineDisplacement: '',
      HorsePower: '',
      Mileage: '',
      Colour: '',
      VinNo: '',
      Fuel: '',
      BodyType: '',
      SteeringWheel: '',
      Condition: '',
      Transmission: '',
      Drive: '',
      Country: '',
      RegisteredIn: '',
      DoorsNo: '',
      SeatsNo: '',
      ElectricMirrors: false,
      ElectricWindow: false,
      AirConditioning: false,
      CruiseControl: false,
      LeatherSeats: false,
      HeatedSeats: false,
      SunRoof: false,
      Description: '',
      Photos: '',
      City: '',
      PhoneNo: '',
      Voivodeship: '',
      UserID: '',
      Email: '',
      Guid: this.globalService.newGuid(),
      Image1: '8f181d26f8c60a122136993a6d60f7c.jpg',
      Image2: '8f181d26f8c60a122136993a6d60f7c.jpg',
      Image3: '8f181d26f8c60a122136993a6d60f7c.jpg',
      Image4: '8f181d26f8c60a122136993a6d60f7c.jpg',
      Image5: '8f181d26f8c60a122136993a6d60f7c.jpg',
      Image6: '8f181d26f8c60a122136993a6d60f7c.jpg',
      Image7: '8f181d26f8c60a122136993a6d60f7c.jpg',
      Image8: '8f181d26f8c60a122136993a6d60f7c.jpg',
      UserAccept: false,
      AdminAccept: false,
      IsPromoted: false,
      StartDate: this.date,
      EndDate: '',
      IsArchival: false,
      IsDeleted: false,
      DisplayCounter: 0,
    });

    this.modelForm.valueChanges.subscribe((value: FormGroup) => {
      this.onControlValueChanged();
    });
    this.onControlValueChanged();


    this.patchValue();
    this.patchArray();
  }

  // Validation method
  onControlValueChanged(): void {
    const form = this.modelForm;
    // tslint:disable:forin
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (this.validationParametr === false) {
        if (control && control.dirty && !control.valid) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      } else {
        if (control) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      }
    }
  }

  checkValidation(stepper: MatStepper): void {
    stepper.selectedIndex = 0;
    this.validationParametr = true;
    const form = this.modelForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }

  patchValue() {
    const token = this.cookieService.get('token');
    this.userService.getUserByToken(this.cookieService.get('token')).subscribe(
      data => {
        if (data) {
          this.email = data[0].email;
          this.modelForm.get('Email').setValue(data[0].email);
          this.modelForm.get('UserID').setValue(data[0].id);
        }
      });
  }

  patchArray() {
    this.marki = this.globalService.marki;
    this.modeleTemp = this.globalService.modeleTemp;
    this.years = this.globalService.years;
    this.typyNadwozia = this.globalService.typyNadwozia;
    this.rodzajPaliwa = this.globalService.rodzajPaliwa;
    this.style = this.globalService.style;
    this.kierownice = this.globalService.kierownice;
    this.stany = this.globalService.stany;
    this.skrzynie = this.globalService.skrzynie;
    this.napedy = this.globalService.napedy;
    this.kraje = this.globalService.kraje;
    this.liczbaDrzwi = this.globalService.liczbaDrzwi;
    this.wojewodztwa = this.globalService.wojewodztwa;
    this.kolory = this.globalService.kolory;
  }

  toogleSell() {
    this.isSell = !this.isSell;
  }

  toogleRent() {
    this.isRent = !this.isRent;
  }

  toogleMoreData() {
    this.isMoreData = !this.isMoreData;
  }

  setModel(marka: string) {
    this.modele = [{ value: 'Wszystkie', viewValue: 'Wszystkie' }];
    for (const model of this.modeleTemp) {
      if (model.value === marka) {
        this.modele.push(model);
      }
    }
  }

  onUpload(file: UploadedFile, image: string) {
    const splitted = file.response.split('"');
    this.modelForm.controls[image].setValue(splitted[9]);
  }

  onSubmit() {
    this.noticeService.addNotice(this.modelForm.value)
      .subscribe(result => {
        this.router.navigate(['/Ogloszenie/' + this.modelForm.value.Guid], { queryParams: { userAccept: 'true' } });
      });
  }

  // end
}
