import { Component, OnInit } from '@angular/core';
import { NoticeService } from '../services/notice.service';
import { Notice } from '../GlobalDataModel/notice';
import { Search } from '../GlobalDataModel/search';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  promoted = true;
  notices: Notice[];
  noData = false;

  //  time: Observable<string>;


  constructor(
    private noticeService: NoticeService,
  ) { }

  ngOnInit() {
   }

  search(search: Search) {
    this.promoted = false;
    this.noticeService.searchNoticeList(search).subscribe(
      data => {
        this.notices = data;
        if (data.length === 0) {
          this.notices = [];
          this.noData = true;
        } else { this.noData = false; }
      });
  }

  goToSponsor() {
    // window.location.href='http://www.reakcjamotorsport.pl/';
    window.open("http://www.reakcjamotorsport.pl", "_blank");
  }
}
