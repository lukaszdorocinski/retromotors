import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Select } from '../GlobalDataModel/Select';
import { Router } from '@angular/router';
import { MailService } from '../services/mail.service';
import { RegisterService } from '../services/register.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  modelForm: FormGroup;
  select: Select;
  status: string;
  message: '';
  hide = true; // do wpisywania hasła
  validationParametr = false; // parametr for check validation fields

  formErrors = {
    login: '',
    password: '',
    statment: '',
    recaptchaReactive: '',
  };

  private validationMessages = {
    login: {
      required: 'Podanie E-Maila jest wymagane'
    },
    password: {
      required: 'Podanie hasła jest wymagane'
    },
    statment: {
      required: 'Zapoznanie się z Regulaminem i zaznaczenie oświadczenia jest wymagane'
    },
    recaptchaReactive: {
      required: 'Jeśli jesteś człowiekiem to wiesz co masz zrobić'
    },
  };

  constructor(
    private formBuilder: FormBuilder,
    private registerService: RegisterService,
    private authService: AuthenticationService,
    private router: Router,
    private mailService: MailService,
  ) { }

  ngOnInit() {
    this.modelForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirm: '',
      statment: [false, Validators.requiredTrue],
      recaptchaReactive: ['', Validators.requiredTrue],
    }, {
        validator: this.comparePassword.bind(this)
      });

    this.modelForm.valueChanges.subscribe((value: FormGroup) => {
      this.onControlValueChanged();
    });
    this.onControlValueChanged();
  }

  // Validation method
  onControlValueChanged(): void {
    const form = this.modelForm;
    // tslint:disable:forin
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (this.validationParametr === false) {
        if (control && control.dirty && !control.valid) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      } else {
        if (control) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      }
    }
  }

  checkValidation(): void {
    this.validationParametr = true;
    const form = this.modelForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }

  comparePassword(group: FormGroup) {
    if (group.value.password === group.value.passwordConfirm) {
      return null;
    } else {
      return {
        invalidComparePassword: true
      };
    }
  }

  public resolved(captchaResponse: string) {
    const recaptchaReactive = this.modelForm.controls['recaptchaReactive'];
    recaptchaReactive.clearValidators();
    recaptchaReactive.updateValueAndValidity();
  }

  // other
  goToTerms() {
    this.router.navigate(['/Regulamin']);
  }

  onSubmit(form: FormGroup) {
    const login = form.get('login').value;
    const password = form.get('password').value;
    const passwordConfirm = form.get('passwordConfirm').value;
    if (this.modelForm.value.statment === false) {
      this.status = 'Hasła muszą być zgodne!';
    }
    if (password === passwordConfirm) {
      const select = { email: login, password: password };
      this.registerService.addUser(select).subscribe(
        data => {
          // console.log(data);
          if (data.error === '') {
            this.status = 'Zarejestrowano użytkownika';
            const selectLogin = { table: 'users', bind: ['id', 'email'], where: { email: login, password: password } };
            this.authService.getSaltByEmail(login)
              .subscribe(
                // tslint:disable-next-line:no-shadowed-variable
                data => {
                  this.mailService.sendRegistrationMail(login, data[0].salt)
                  .subscribe((response: Response) => {
                    // console.log(response);
                    this.router.navigate(['/']);
                  } );
                  // this.router.navigate(['/']);
                });
          }
          if (data.error === 'email in use!') {
            this.status = 'Login/Email w użyciu';
          }
        });
    } else {
      this.status = 'Hasła muszą być zgodne!';
    }
  }
  // end
}
