import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmationComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const salt = params['guid'];
      const select = { table: 'users', bind: { status: 1 }, where: { salt: salt } };
      this.userService.updateUser(select)
        .subscribe(
        data => {
          console.log(data);
          if (data === true) {
            // console.log('aktywowane');
          } else {
            // console.log('błąd');
          }
        });
    });
  }

}
