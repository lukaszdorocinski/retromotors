import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { NgcCookieConsentService, NgcStatusChangeEvent, NgcInitializeEvent } from 'ngx-cookieconsent';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy  {
  title = 'RetroMotors';
  username = '';
  firstLetter = '';

    // keep refs to subscriptions to be able to unsubscribe later
    private popupOpenSubscription: Subscription;
    private popupCloseSubscription: Subscription;
    private initializeSubscription: Subscription;
    private statusChangeSubscription: Subscription;
    private revokeChoiceSubscription: Subscription;

  constructor(
    private authService: AuthenticationService,
    private userService: UserService,
    private ccService: NgcCookieConsentService
  ) { }

  ngOnInit() {
    if (!this.authService.isLoggedIn) {
      this.authService.setLoalStorage();
    }

        // subscribe to cookieconsent observables to react to main events
        this.popupOpenSubscription = this.ccService.popupOpen$.subscribe(
          () => {
            // you can use this.ccService.getConfig() to do stuff...
          });

        this.popupCloseSubscription = this.ccService.popupClose$.subscribe(
          () => {
            // you can use this.ccService.getConfig() to do stuff...
          });

        this.initializeSubscription = this.ccService.initialize$.subscribe(
          (event: NgcInitializeEvent) => {
            // you can use this.ccService.getConfig() to do stuff...
          });

        this.statusChangeSubscription = this.ccService.statusChange$.subscribe(
          (event: NgcStatusChangeEvent) => {
            // you can use this.ccService.getConfig() to do stuff...
          });

        this.revokeChoiceSubscription = this.ccService.revokeChoice$.subscribe(
          () => {
            // you can use this.ccService.getConfig() to do stuff...
          });
  }

  ngOnDestroy() {
    // unsubscribe to cookieconsent observables to prevent memory leaks
    this.popupOpenSubscription.unsubscribe();
    this.popupCloseSubscription.unsubscribe();
    this.initializeSubscription.unsubscribe();
    this.statusChangeSubscription.unsubscribe();
    this.revokeChoiceSubscription.unsubscribe();
  }

  public isLoggedIn() {
    // if (this.sharedService.globalUser.nick_name !== '') {
    //   this.username = this.sharedService.globalUser.nick_name;
    //   return true;
    // } else {
    //   if (this.sharedService.globalUser.email !== '') {
    //     this.username = this.sharedService.globalUser.email;
    //     return true;
    //   } else { return false; }
    // }
    const userData = JSON.parse(localStorage.getItem('currentUser'));
    if (userData) {
      if (userData.nickname !== null) {
        this.username = userData.nickname;
      } else { this.username = userData.username; }
      return true;
    } else {
      return false;
    }
  }

  Logout() {
    // console.log('logout');
    this.authService.logout();
  }
}
