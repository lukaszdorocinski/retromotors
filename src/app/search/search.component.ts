import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { DateAdapter, NativeDateAdapter } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';

import { Select } from '../GlobalDataModel/Select';
import { Search } from '../GlobalDataModel/search';
import { Router } from '@angular/router';
import { NoticeService } from '../services/notice.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Output() onProp = new EventEmitter<Search>();

  panelOpenState = false;
  select: Select;
  selectedValue: string;

  marki: any[];
  modele: any[];
  modeleTemp: any[];
  years: any[];
  typyNadwozia: any[];
  rodzajPaliwa: any[];
  wojewodztwa: any[];
  style: any[];
  kolory: any[];

  modelForm: FormGroup;
  modelRentalForm: FormGroup;
  disabledModel = true;

  getData: string;

  constructor(
    private formBuldier: FormBuilder,
    private noticeService: NoticeService,
    private router: Router,
    private globalService: GlobalService,
    // dateAdapter: DateAdapter<NativeDateAdapter>,
  ) {
    // dateAdapter.setLocale('gb-GB');
  }

  ngOnInit() {
    this.modelForm = this.formBuldier.group({
      query: '',
      Voivodeship: '',
      marka: 'Marka',
      model: 'Model',
      rokProdukcjiOd: '',
      rokProdukcjiDo: '',
      cenaOd: '',
      cenaDo: '',
      typNadwozia: '',
      paliwo: '',
      kolor: '',
      styl: '',
      isSelling: false,
      isRental: false,
    });

    this.modelRentalForm = this.formBuldier.group({
      query: '',
      Voivodeship: '',
      marka: 'Marka',
      model: 'Model',
      rokProdukcjiOd: '',
      rokProdukcjiDo: '',
      cenaOd: '',
      cenaDo: '',
      typNadwozia: '',
      paliwo: '',
      kolor: '',
      styl: '',
      isSelling: false,
      isRental: false,
    });
    this.patchArray();
    // ustawienie modelu w zależności od marki
    this.setModel('Wszystkie');
  }

  patchArray() {
    this.marki = this.globalService.marki;
    this.modeleTemp = this.globalService.modeleTemp;
    this.years = this.globalService.years;
    this.typyNadwozia = this.globalService.typyNadwozia;
    this.rodzajPaliwa = this.globalService.rodzajPaliwa;
    this.style = this.globalService.style;
    this.kolory = this.globalService.kolory;
    this.wojewodztwa = this.globalService.wojewodztwa;
  }

  setModel(marka: string) {
    this.modele = [{ value: 'Wszystkie', viewValue: 'Wszystkie' }];
    for (const model of this.modeleTemp) {
      if (model.value === marka) {
        this.modele.push(model);
      }
    }
  }

  onSubmit(form: FormGroup, valueS: string) {
    if (valueS === 'isSelling') {
      form.value.isSelling = true;
      form.value.isRental = false;
    }
    if (valueS === 'isRental') {
      form.value.isRental = true;
      form.value.isSelling = false;
    }
    this.onProp.emit(form.value);
  }

}
