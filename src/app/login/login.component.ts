import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Select } from '../GlobalDataModel/Select';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  modelForm: FormGroup;
  select: Select;
  hide = true; // or password entry
  loading = false;
  error = '';

  validationParametr = false; // parametr for check validation fields

  formErrors = {
    login: '',
    password: '',
  };

  private validationMessages = {
    login: {
      required: 'Podanie loginu jest wymagane'
    },
    password: {
      required: 'Podanie hasła jest wymagane'
    },
  };

  constructor(
    private formBuldier: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit() {
    // this.authService.logout();

    this.modelForm = this.formBuldier.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.modelForm.valueChanges.subscribe((value: FormGroup) => {
      this.onControlValueChanged();
  });
  this.onControlValueChanged();
  }

  // Validation method
  onControlValueChanged(): void {
    const form = this.modelForm;
    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let control = form.get(field);
      if (this.validationParametr == false) {
        if (control && control.dirty && !control.valid) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      }
      else {
        if (control) {
          const validationMessages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      }
    }
  }

  checkValidation(): void {
    this.validationParametr = true;
    const form = this.modelForm;
    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      let control = form.get(field);

      if (control) {
        const validationMessages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }

  // other metods
  goToTerms() {
    this.router.navigate(['/Regulamin']);
  }
  goToRegister() {
    this.router.navigate(['/Rejestracja']);
  }

  onSubmit(form: FormGroup) {
    const select = { email: form.value.login, password: form.value.password };
    this.authService.login(select).subscribe(
      data => { if (data.status === true) { this.router.navigate(['/']); } });
  }

}



