
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TermsComponent } from './terms/terms.component';
import { UserPanelComponent } from './user-panel/user-panel.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { NoticeComponent } from './notice/notice.component';
import { NoticeListComponent } from './notice-list/notice-list.component';
import { UserResolve } from './services/user-resolve.services';
import { NoticeAddComponent } from './notice-add/notice-add.component';
import { PromotedComponent } from './promoted/promoted.component';
import { Component } from '@angular/core/src/metadata/directives';
import { Search } from './GlobalDataModel/search';
import { AuthGuard } from './GlobalDataModel/auth.guard';
import { NoticeResolve } from './services/notice-resolve.services';
import { NoticeEditComponent } from './notice-edit/notice-edit.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ContactComponent } from './contact/contact.component';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { AssistanceComponent } from './assistance/assistance.component';

const routes: Routes = [
  { path: '', redirectTo: '/Index', pathMatch: 'full' },
  { path: 'Index',
  component: IndexComponent,
  children: [
    {path: '', component: PromotedComponent},
    {path: 'Promoted', component: PromotedComponent },
    {path: 'NoticeList', component: NoticeListComponent, runGuardsAndResolvers: 'always', }
  ]
},
  { path: 'Logowanie',  component: LoginComponent },
  { path: 'Rejestracja',  component: RegisterComponent },
  { path: 'Regulamin',  component: TermsComponent },
  { path: 'UserPanel',  component: UserPanelComponent, canActivate: [AuthGuard] },
  { path: 'UserDetails',  component: UserDetailsComponent , canActivate: [AuthGuard]},
  // { path: 'UserDetails/:id', component: UserDetailsComponent, resolve: { user: UserResolve }, canActivate: [AuthGuard] },
  { path: 'Ogloszenie/:guid',  component: NoticeComponent, resolve: { notice: NoticeResolve }, },
  { path: 'OgloszeniePromowane/:guid',  component: NoticeComponent, resolve: { notice: NoticeResolve }, },
  { path: 'NoticeList',  component: NoticeListComponent },
  { path: 'NoticeAdd',  component: NoticeAddComponent, canActivate: [AuthGuard] },
  { path: 'NoticeEdit/:guid',  component: NoticeEditComponent, resolve: { notice: NoticeResolve }, },
  { path: 'Confirm/:guid', component: ConfirmationComponent },
  { path: 'Kontakt', component: ContactComponent },
  { path: 'Polityka-Cookie', component: CookiePolicyComponent },
  { path: 'Reklama-Na-Stronie', component: AdvertisementComponent },
  { path: 'Pomoc', component: AssistanceComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
