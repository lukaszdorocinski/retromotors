import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { GlobalService } from './global.service';

@Injectable()
export class MailService {

  constructor(
    private http: Http,
    private router: Router,
    private globalService: GlobalService,
  ) { }

  sendRegistrationMail(mailAdress: string, guid: string): Observable<any> {
    const mailSubject = 'Wymagane potwierdzenie rejestracji w serwisie RetroMotors.pl';
    // tslint:disable-next-line:max-line-length
    const mailBody = '<h2>Potwierdź rejestrację konta</h2></br> Aby dokończyć rejestrację konta kliknij w link: <a href="http://retromotors.pl/Confirm/'
    // tslint:disable-next-line:max-line-length
    + guid + '"> Potwierdź rejestrację </a>. <br> Po kliknięciu w link konto będzie aktywne, i możliwe będzie zalogowanie się do systemu. <br> Pozdrawiamy, <br> Zespół RetroMotors'
    // tslint:disable-next-line:max-line-length
    + '<br><br><span style="color: rgb(187, 187, 187);"><br><font size="1">Wiadomość została wysłana automatycznie, nie należy na nią odpowiadać'
    // tslint:disable-next-line:max-line-length
    + 'Jeśli otrzymałeś/aś tę wiadomość przez pomyłkę i nie zamierzasz tworzyć konta w serwisie RetroMotors.pl, zignoruj tego maila.</font></span>';
    const select = { target: mailAdress, subject: mailSubject, body: mailBody};
    const body = JSON.stringify(select);
    // console.log(body);
    const username = this.globalService.username;
    const password = this.globalService.password;
    const headers = new Headers();
    headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({ headers: headers });
    return this.http.post(this.globalService.mail, body, options)
    .map((response: Response) => {
      // tslint:disable-next-line:no-unused-expression
      <any>response.json();
      // console.log(response);
    });

  }
// end
}
