import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { GlobalService } from './global.service';

@Injectable()
export class RegisterService {

    constructor(
        private http: Http,
        private globalService: GlobalService
    ) { }

    addUser(select: any) {
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.registry, body, options
        )
            .map((response: Response) => <any>response.json());
    }
    // end
}
