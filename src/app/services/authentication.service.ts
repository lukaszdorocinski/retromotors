import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, Jsonp, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { GlobalService } from './global.service';
import { User } from '../GlobalDataModel/user.model';

@Injectable()
export class AuthenticationService {

    public token: string;
    public isLoggedIn: boolean;

    constructor(
        private http: Http,
        private userService: UserService,
        private cookieService: CookieService,
        private router: Router,
        private globalService: GlobalService,
    ) {
        // set token if saved in local storage
        // const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // this.token = currentUser && currentUser.token;
    }

    login(select: any): Observable<any> {
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.login, body, options)
            .map((response: Response) => {
                if (response.json().status === true) {
                    const token = response.json() && response.json().session_hash;
                    this.token = token;
                    this.userService.getUserByToken(token).subscribe(dataServ => {
                        // console.log(dataServ);
                        const nickname = dataServ[0].nick_name;
                        localStorage.setItem('currentUser', JSON.stringify({ username: select.email, nickname: nickname }));
                        // localStorage.setItem('token', token);
                        // this.cookieService.set( 'currentUser', JSON.stringify({ username: select.email, nickname: nickname, token: token }));
                        this.cookieService.set('token', token, 40, '/');
                        this.isLoggedIn = true;
                    });
                }
                return <any>response.json();
            });
    }

    getSaltByEmail(mail: string): Observable<User> {
        const select = { table: 'users', bind: ['salt'], where: { email: mail } };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <any>response.json());
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
        // this.cookieService.delete('currentUser');
        this.cookieService.delete('token');
        this.router.navigate(['/']);
    }

    setLoalStorage() {
        if (this.cookieService.check('token')) {
            this.userService.getUserByToken(this.cookieService.get('token')).subscribe(data => {
                if (data) {
                    localStorage.setItem('currentUser', JSON.stringify({ username: data[0].email, nickname: data[0].nick_name }));
                    this.isLoggedIn = true;
                }
            });
        }
    }

}
