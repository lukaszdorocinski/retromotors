import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { User } from '../GlobalDataModel/user.model';
import { UserService } from './user.service';

@Injectable()
export class UserResolve implements Resolve<User> {
    constructor(private userService: UserService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.userService.getUserById(route.params['id']);
    }
}

// @Injectable()
// export class DelegacjeListResolve implements Resolve<string[]> {
//     constructor(private delegacjeService: DelegacjeService) { }

//     resolve(route: ActivatedRouteSnapshot) {
//         return this.delegacjeService.getGetPoziomyAkceptacjiDokumentowList();
//     }
// }
