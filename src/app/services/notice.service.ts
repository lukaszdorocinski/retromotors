import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Promise } from 'q';
import { Notice } from '../GlobalDataModel/notice';
import { Search } from '../GlobalDataModel/search';
import * as _ from 'underscore';
import { GlobalService } from './global.service';

@Injectable()
export class NoticeService {
    // tslint:disable:max-line-length

    searchModel: Search;
    data: any[];

    @Output() change: EventEmitter<Search> = new EventEmitter();

    constructor(
        private http: Http,
        private globalService: GlobalService,
    ) { }

    searchEmit(search: Search) {
        this.searchModel = search;
        this.change.emit(search);
    }

    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 20) {
        // calculate total pages
        const totalPages = Math.ceil(totalItems / pageSize);

        let startPage: number, endPage: number;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        // calculate start and end item indexes
        const startIndex = (currentPage - 1) * pageSize;
        const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        const pages = _.range(startPage, endPage + 1);
        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

    addNotice(select: any) {
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.noticeadd, body, options
        ).map(this.extractData);
    }

    updateNotice(select: any): Observable<any> {
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.update, body, options)
            .map(response => {
                if (response.status < 200 || response.status >= 300) {
                    return false;
                } else {
                    this.data = response.json();
                    if (this.data.length === 0) {
                        return false;
                    } else {
                        return true;
                    }
                }
            });
    }

    getAllNotice(): Observable<Notice[]> {
        const select = {
            table: 'notice', bind: ['Id', 'NoticeTitle', 'SellingPrice', 'IsSelling', 'RentalPrice', 'IsRental', 'Mark', 'Model', 'Style',
                'ProductionYear', 'EngineDisplacement', 'HorsePower', 'Mileage', 'Colour', 'VinNo', 'Fuel', 'BodyType', 'SteeringWheel', 'Condition', 'Transmission',
                'Drive', 'Country', 'RegisteredIn', 'DoorsNo', 'SeatsNo', 'ElectricMirrors', 'ElectricWindow', 'AirConditioning', 'CruiseControl', 'LeatherSeats',
                'HeatedSeats', 'SunRoof', 'Description', 'Photos', 'City', 'PhoneNo', 'Voivodeship', 'UserID', 'Guid', 'Image1', 'Email', 'UserAccept', 'AdminAccept',
                'IsPromoted', 'StartDate', 'EndDate', 'IsArchival', 'IsDeleted', 'DisplayCounter', 'Image2', 'Image3', 'Image4', 'Image5', 'Image6', 'Image7', 'Image8']
        };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <Notice[]>response.json());
    }

    getAllActiveNotice(): Observable<Notice[]> {
        const select = {
            table: 'notice', bind: ['Id', 'NoticeTitle', 'SellingPrice', 'IsSelling', 'RentalPrice', 'IsRental', 'Mark', 'Model', 'Style',
                'ProductionYear', 'EngineDisplacement', 'HorsePower', 'Mileage', 'Colour', 'VinNo', 'Fuel', 'BodyType', 'SteeringWheel', 'Condition', 'Transmission',
                'Drive', 'Country', 'RegisteredIn', 'DoorsNo', 'SeatsNo', 'ElectricMirrors', 'ElectricWindow', 'AirConditioning', 'CruiseControl', 'LeatherSeats',
                'HeatedSeats', 'SunRoof', 'Description', 'Photos', 'City', 'PhoneNo', 'Voivodeship', 'UserID', 'Guid', 'Image1', 'Email', 'UserAccept', 'AdminAccept',
                'IsPromoted', 'StartDate', 'EndDate', 'IsArchival', 'IsDeleted', 'DisplayCounter', 'Image2', 'Image3', 'Image4', 'Image5', 'Image6', 'Image7', 'Image8'],
            where: { UserAccept: 1 }
        };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <Notice[]>response.json());
    }

    getNoticeById(id: number): Observable<Notice> {
        const select = {
            table: 'notice', bind: ['Id', 'NoticeTitle', 'SellingPrice', 'IsSelling', 'RentalPrice', 'IsRental', 'Mark', 'Model', 'Style',
                'ProductionYear', 'EngineDisplacement', 'HorsePower', 'Mileage', 'Colour', 'VinNo', 'Fuel', 'BodyType', 'SteeringWheel', 'Condition', 'Transmission',
                'Drive', 'Country', 'RegisteredIn', 'DoorsNo', 'SeatsNo', 'ElectricMirrors', 'ElectricWindow', 'AirConditioning', 'CruiseControl', 'LeatherSeats',
                'HeatedSeats', 'SunRoof', 'Description', 'Photos', 'City', 'PhoneNo', 'Voivodeship', 'UserID', 'Guid', 'Image1', 'Email', 'UserAccept', 'AdminAccept',
                'IsPromoted', 'StartDate', 'EndDate', 'IsArchival', 'IsDeleted', 'DisplayCounter', 'Image2', 'Image3', 'Image4', 'Image5', 'Image6', 'Image7', 'Image8'],
            where: { Id: id }
        };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <any>response.json());
    }

    getNoticeByUserID(userId: number): Observable<Notice[]> {
        const select = {
            table: 'notice', bind: ['Id', 'NoticeTitle', 'SellingPrice', 'IsSelling', 'RentalPrice', 'IsRental', 'Mark', 'Model', 'Style',
                'ProductionYear', 'EngineDisplacement', 'HorsePower', 'Mileage', 'Colour', 'VinNo', 'Fuel', 'BodyType', 'SteeringWheel', 'Condition', 'Transmission',
                'Drive', 'Country', 'RegisteredIn', 'DoorsNo', 'SeatsNo', 'ElectricMirrors', 'ElectricWindow', 'AirConditioning', 'CruiseControl', 'LeatherSeats',
                'HeatedSeats', 'SunRoof', 'Description', 'Photos', 'City', 'PhoneNo', 'Voivodeship', 'UserID', 'Guid', 'Image1', 'Email', 'UserAccept', 'AdminAccept',
                'IsPromoted', 'StartDate', 'EndDate', 'IsArchival', 'IsDeleted', 'DisplayCounter', 'Image2', 'Image3', 'Image4', 'Image5', 'Image6', 'Image7', 'Image8'],
            where: { UserID: userId }
        };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <any>response.json());
    }

    getNoticeByGuid(guid: string): Observable<Notice> {
        const select = {
            table: 'notice', bind: ['Id', 'NoticeTitle', 'SellingPrice', 'IsSelling', 'RentalPrice', 'IsRental', 'Mark', 'Model', 'Style',
                'ProductionYear', 'EngineDisplacement', 'HorsePower', 'Mileage', 'Colour', 'VinNo', 'Fuel', 'BodyType', 'SteeringWheel', 'Condition', 'Transmission',
                'Drive', 'Country', 'RegisteredIn', 'DoorsNo', 'SeatsNo', 'ElectricMirrors', 'ElectricWindow', 'AirConditioning', 'CruiseControl', 'LeatherSeats',
                'HeatedSeats', 'SunRoof', 'Description', 'Photos', 'City', 'PhoneNo', 'Voivodeship', 'UserID', 'Guid', 'Image1', 'Email', 'UserAccept', 'AdminAccept',
                'IsPromoted', 'StartDate', 'EndDate', 'IsArchival', 'IsDeleted', 'DisplayCounter', 'Image2', 'Image3', 'Image4', 'Image5', 'Image6', 'Image7', 'Image8'],
            where: { Guid: guid }
        };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <any>response.json());
    }

    searchNoticeList(searchSelect: Search): Observable<Notice[]> {
        const select = {
            query: searchSelect.query,
            SellingPrice: searchSelect.cenaOd,
            Mark: searchSelect.marka,
            Model: searchSelect.model
        };
        let body = JSON.stringify(select);
        body = this.przygotujZapytanie(searchSelect);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.search, body, options)
            .map((response: Response) => <Notice[]>response.json());
    }

    przygotujZapytanie(searchSelect: Search): string {
        let body = '{';
        // sprzedaż wynajem
        if (searchSelect.isRental === true) {
            body = body + '"IsRental":"1"';
        }
        if (searchSelect.isSelling === true) {
            body = body + '"IsSelling":"1"';
        }
        // query
        if (searchSelect.query !== '') {
            body = body + ',"query":"' + searchSelect.query + '"';
        }
        // zapytania ograniczające
        if (searchSelect.marka !== 'Marka') {
            if (searchSelect.marka !== 'Wszystkie') {
                body = body + ',"Mark":"' + searchSelect.marka + '"';
            }
        }
        if (searchSelect.model !== 'Model') {
            if (searchSelect.model !== 'Wszystkie') {
                body = body + ',"Model":"' + searchSelect.model + '"';
            }
        }
        if (searchSelect.cenaOd.toString() !== '') {
            if (searchSelect.cenaOd.toString() !== 'Wszystkie') {
                body = body + ',"SellingPriceFrom":"' + searchSelect.cenaOd + '"';
            }
        }
        if (searchSelect.cenaDo.toString() !== '') {
            if (searchSelect.cenaDo.toString() !== 'Wszystkie') {
                body = body + ',"SellingPriceTo":"' + searchSelect.cenaDo + '"';
            }
        }
        if (searchSelect.rokProdukcjiOd.toString() !== '') {
            if (searchSelect.rokProdukcjiOd.toString() !== 'Wszystkie') {
                body = body + ',"YearFrom":"' + searchSelect.rokProdukcjiOd + '"';
            }
        }
        if (searchSelect.rokProdukcjiDo.toString() !== '') {
            if (searchSelect.rokProdukcjiDo.toString() !== 'Wszystkie') {
                body = body + ',"YearTo":"' + searchSelect.rokProdukcjiDo + '"';
            }
        }
        if (searchSelect.typNadwozia !== '') {
            if (searchSelect.typNadwozia !== 'Wszystkie') {
                body = body + ',"BodyType":"' + searchSelect.typNadwozia + '"';
            }
        }
        if (searchSelect.paliwo !== '') {
            if (searchSelect.paliwo !== 'Wszystkie') {
                body = body + ',"Fuel":"' + searchSelect.paliwo + '"';
            }
        }
        if (searchSelect.Voivodeship !== '') {
            if (searchSelect.Voivodeship !== 'Wszystkie') {
                body = body + ',"Voivodeship":"' + searchSelect.Voivodeship + '"';
            }
        }
        if (searchSelect.kolor !== '') {
            if (searchSelect.kolor !== 'Wszystkie') {
                body = body + ',"Colour":"' + searchSelect.kolor + '"';
            }
        }
        if (searchSelect.styl !== '') {
            if (searchSelect.styl !== 'Wszystkie') {
                body = body + ',"Style":"' + searchSelect.styl + '"';
            }
        }
        body = body + '}';
        return body;
    }

    fileChange(event: any) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const formData: FormData = new FormData();
            formData.append('uploads', file, file.name);
            const username = this.globalService.username;
            const password = this.globalService.password;
            const headers = new Headers();
            headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
            const options = new RequestOptions({ headers: headers });
            this.http.post(this.globalService.upload, formData, options)
                .map(res => res.json())
                .subscribe(
                    data => console.log('data')
                );
        } else { console.log('error else'); }
    }

    private extractData(res: Response) {
        return res.text() ? res.json() : {};
    }

    private extractError(res: Response) {
        const errMsg = 'Error received from the API';
        return errMsg;
    }

    private handleSuccess(response) {
        console.log('Successfully uploaded image');
        // provide your own implementation of handling the response from API
    }

    private handleError(errror) {
        console.error('Error uploading image');
    }
    // end
}

