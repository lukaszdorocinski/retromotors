import { Injectable } from '@angular/core';
import { Notice } from '../GlobalDataModel/notice';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class GlobalService {

// tslint:disable:member-ordering
  username = 'lukas_dorocinski';
  password = '5rFTsaFNnaDXDF8z';

  login = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/login';
  select = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/select';
  update = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/update';
  mail = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/mail';
  registry = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/registry';
  noticeadd = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/noticeadd';
  upload = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/upload';
  search = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/search';

  constructor(
    private cookieService: CookieService,
  ) { }

checkCookiePolicy() : boolean {
  const cookieExists: boolean = this.cookieService.check('zgadzamSieNaPolitykePrywatnosciRetroMotors');
  console.log(cookieExists);
  return cookieExists;
}

setCookiePolicy() {
  this.cookieService.set('zgadzamSieNaPolitykePrywatnosciRetroMotors', 'true', 60, '/');
}

delCoookiePolicy() {
  this.cookieService.delete('zgadzamSieNaPolitykePrywatnosciRetroMotors');

}
  // zamiana 0 i 1 na false true
  public booleanParse(notice: Notice) {
    notice.IsSelling = !!+notice.IsSelling;
    notice.IsRental = !!+notice.IsRental;
    notice.ElectricMirrors = !!+notice.ElectricMirrors;
    notice.ElectricWindow = !!+notice.ElectricWindow;
    notice.AirConditioning = !!+notice.AirConditioning;
    notice.CruiseControl = !!+notice.CruiseControl;
    notice.LeatherSeats = !!+notice.LeatherSeats;
    notice.HeatedSeats = !!+notice.HeatedSeats;
    notice.SunRoof = !!+notice.SunRoof;
    notice.UserAccept = !!+notice.UserAccept;
    notice.AdminAccept = !!+notice.AdminAccept;
    notice.IsPromoted = !!+notice.IsPromoted;
    notice.IsArchival = !!+notice.IsArchival;
    notice.IsDeleted = !!+notice.IsDeleted;
    return notice;
    }

    public marki = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Alfa Romeo', viewValue: 'Alfa Romeo' },
      { value: 'Audi', viewValue: 'Audi' },
      { value: 'Austin', viewValue: 'Austin' },
      { value: 'Bentley', viewValue: 'Bentley' },
      { value: 'BMW', viewValue: 'BMW' },
      { value: 'Buick', viewValue: 'Buick' },
      { value: 'Cadilac', viewValue: 'Cadilac' },
      { value: 'Chevrolet', viewValue: 'Chevrolet' },
      { value: 'Chrysler', viewValue: 'Chrysler' },
      { value: 'Citroen', viewValue: 'Citroen' },
      { value: 'Dodge', viewValue: 'Dodge' },
      { value: 'Fiat', viewValue: 'Fiat' },
      { value: 'Ford', viewValue: 'Ford' },
      { value: 'Gaz', viewValue: 'Gaz' },
      { value: 'Honda', viewValue: 'Honda' },
      { value: 'Jaguar', viewValue: 'Jaguar' },
      { value: 'Jeep', viewValue: 'Jeep' },
      { value: 'Lexus', viewValue: 'Lexus' },
      { value: 'Lancia', viewValue: 'Lancia' },
      { value: 'Lincoln', viewValue: 'Lincoln' },
      { value: 'Łada', viewValue: 'Łada' },
      { value: 'Maserati', viewValue: 'Maserati' },
      { value: 'Mazda', viewValue: 'Mazda' },
      { value: 'Mercedes-Benz', viewValue: 'Mercedes-Benz' },
      { value: 'Oldsmobile', viewValue: 'Oldsmobile' },
      { value: 'Opel', viewValue: 'Opel' },
      { value: 'Reugeot', viewValue: 'Reugeot' },
      { value: 'Pontiac', viewValue: 'Pontiac' },
      { value: 'Porsche', viewValue: 'Porsche' },
      { value: 'Renault', viewValue: 'Renault' },
      { value: 'Rolls-Royce', viewValue: 'Rolls-Royce' },
      { value: 'Saab', viewValue: 'Saab' },
      { value: 'Skoda', viewValue: 'Skoda' },
      { value: 'Toyota', viewValue: 'Skoda' },
      { value: 'Vauxhall', viewValue: 'Vauxhall' },
      { value: 'Volvo', viewValue: 'Volvo' },
      { value: 'VW', viewValue: 'VW' }
    ];

    public modeleTemp = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Alfa Romeo', viewValue: '33' },
      { value: 'Alfa Romeo', viewValue: '33 Stradale' },
      { value: 'Alfa Romeo', viewValue: '75' },
      { value: 'Alfa Romeo', viewValue: '90' },
      { value: 'Alfa Romeo', viewValue: '155' },
      { value: 'Alfa Romeo', viewValue: '158 Alfetta' },
      { value: 'Alfa Romeo', viewValue: '164' },
      { value: 'Alfa Romeo', viewValue: '1750' },
      { value: 'Alfa Romeo', viewValue: '2600' },
      { value: 'Alfa Romeo', viewValue: 'Alfasud' },
      { value: 'Alfa Romeo', viewValue: 'Alfetta' },
      { value: 'Alfa Romeo', viewValue: 'Arna' },
      { value: 'Alfa Romeo', viewValue: 'Giulia' },
      { value: 'Alfa Romeo', viewValue: 'Giulietta' },
      { value: 'Alfa Romeo', viewValue: 'GTV' },
      { value: 'Alfa Romeo', viewValue: 'Montreal' },
      { value: 'Alfa Romeo', viewValue: 'Spider' },
      { value: 'Alfa Romeo', viewValue: 'Sprint' },
      { value: 'Alfa Romeo', viewValue: 'SZ/RZ' },
      { value: 'Audi', viewValue: '50' },
      { value: 'Audi', viewValue: '80' },
      { value: 'Audi', viewValue: '90' },
      { value: 'Audi', viewValue: '100' },
      { value: 'Audi', viewValue: '200' },
      { value: 'Audi', viewValue: 'Coupe' },
      { value: 'Audi', viewValue: 'RS2' },
      { value: 'Audi', viewValue: 'S2' },
      { value: 'Audi', viewValue: 'Quattro' },
      { value: 'Audi', viewValue: 'V8' },
      { value: 'BMW', viewValue: '1500' },
      { value: 'BMW', viewValue: '1600' },
      { value: 'BMW', viewValue: '1800' },
      { value: 'BMW', viewValue: '2000' },
      { value: 'BMW', viewValue: '3200 CS' },
      { value: 'BMW', viewValue: '850' },
      { value: 'BMW', viewValue: 'E3' },
      { value: 'BMW', viewValue: 'E9' },
      { value: 'BMW', viewValue: 'E12' },
      { value: 'BMW', viewValue: 'E21' },
      { value: 'BMW', viewValue: 'E23' },
      { value: 'BMW', viewValue: 'E24' },
      { value: 'BMW', viewValue: 'E28' },
      { value: 'BMW', viewValue: 'E30' },
      { value: 'BMW', viewValue: 'E32' },
      { value: 'BMW', viewValue: 'E36' },
      { value: 'Fiat', viewValue: 'Fiat' },
      { value: 'Ford', viewValue: 'Ford' },
      { value: 'Honda', viewValue: 'Honda' },
      { value: 'Lancia', viewValue: 'Lancia' },
      { value: 'Mazda', viewValue: 'Mazda' },
      { value: 'Mercedes-Benz', viewValue: 'Mercedes-Benz' },
      { value: 'Opel', viewValue: 'Ascona' },
      { value: 'Opel', viewValue: 'Corsa' },
      { value: 'Opel', viewValue: 'Commodore' },
      { value: 'Opel', viewValue: 'Monza' },
      { value: 'Opel', viewValue: 'Senator' },
      { value: 'Opel', viewValue: 'Rekord' },
      { value: 'Reugeot', viewValue: 'Reugeot' },
      { value: 'Renault', viewValue: 'Renault' },
      { value: 'VW', viewValue: 'VW' }
    ];

    public years = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: '1970', viewValue: '1970 r.' },
      { value: '1971', viewValue: '1971 r.' },
      { value: '1972', viewValue: '1972 r.' },
      { value: '1973', viewValue: '1973 r.' },
      { value: '1974', viewValue: '1974 r.' },
      { value: '1975', viewValue: '1975 r.' },
      { value: '1976', viewValue: '1976 r.' },
      { value: '1977', viewValue: '1977 r.' },
      { value: '1978', viewValue: '1978 r.' },
      { value: '1979', viewValue: '1979 r.' },
      { value: '1980', viewValue: '1980 r.' },
      { value: '1981', viewValue: '1981 r.' },
      { value: '1982', viewValue: '1982 r.' },
      { value: '1983', viewValue: '1983 r.' },
      { value: '1984', viewValue: '1984 r.' },
      { value: '1985', viewValue: '1985 r.' },
      { value: '1986', viewValue: '1986 r.' },
      { value: '1987', viewValue: '1987 r.' },
      { value: '1988', viewValue: '1988 r.' },
      { value: '1989', viewValue: '1989 r.' },
      { value: '1990', viewValue: '1990 r.' },
      { value: '1991', viewValue: '1991 r.' },
      { value: '1992', viewValue: '1992 r.' },
      { value: '1993', viewValue: '1993 r.' },
      { value: '1994', viewValue: '1994 r.' },
      { value: '1995', viewValue: '1995 r.' },
      { value: '1996', viewValue: '1996 r.' },
      { value: '1997', viewValue: '1997 r.' },
    ];

    public typyNadwozia = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Kabriolet', viewValue: 'Kabriolet' },
      { value: 'Sedan', viewValue: 'Sedan' },
      { value: 'Coupe', viewValue: 'Pickup' },
      { value: 'Hatchback', viewValue: 'Hatchback' },
      { value: 'Kombi', viewValue: 'Kombi' },
      { value: 'Terenowy', viewValue: 'Terenowy' },
      { value: 'Minbus', viewValue: 'Minbus' },
      { value: 'SUV', viewValue: 'SUV' },
    ];

    public rodzajPaliwa = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Benzyna', viewValue: 'Benzyna' },
      { value: 'Diesel', viewValue: 'Diesel' },
      { value: 'LPG', viewValue: 'LPG' },
    ];

    public style = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Tuning', viewValue: 'Tuning' },
      { value: 'PRL', viewValue: 'PRL' },
      { value: 'Niemieckie', viewValue: 'Niemieckie' },
      { value: 'Amerykańskie', viewValue: 'Amerykańskie' },
      { value: 'Włoskie', viewValue: 'Włoskie' },
      { value: 'Japońskie', viewValue: 'Japońskie' },
    ];

    public kierownice = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'LHD', viewValue: 'po lewej stronie' },
      { value: 'RHD', viewValue: 'po prawej stronie' },
    ];

    public stany = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'sprawny', viewValue: 'sprawny' },
      { value: 'uszkodzony', viewValue: 'uszkodzony' },
      { value: 'kolekcjonerski', viewValue: 'kolekcjonerski' },
      { value: 'do odbudowy', viewValue: 'do odbudowy' },
    ];

    public skrzynie = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'MT', viewValue: 'manualna' },
      { value: 'AT', viewValue: 'automatyczna' },
    ];

    public napedy = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Front', viewValue: 'na przednią oś' },
      { value: 'Rear', viewValue: 'na tylną oś' },
      { value: 'Full', viewValue: 'na obie osie' },
    ];

    public kraje = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'PL', viewValue: 'Polska' },
      { value: 'USA', viewValue: 'Stany Zjednoczone' },
      { value: 'DE', viewValue: 'Niemcy' },
      { value: 'GB', viewValue: 'Wielka Brytania' },
      { value: 'IT', viewValue: 'Włochy' },
    ];

    public liczbaDrzwi = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 1, viewValue: 1 },
      { value: 2, viewValue: 2 },
      { value: 3, viewValue: 3 },
      { value: 4, viewValue: 4 },
      { value: 5, viewValue: 5 },
    ];

    public wojewodztwa = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Dolnośląskie', viewValue: 'Dolnośląskie' },
      { value: 'Kujawsko-pomorskie', viewValue: 'Kujawsko-pomorskie' },
      { value: 'Lubelskie', viewValue: 'Lubelskie' },
      { value: 'Lubuskie', viewValue: 'Lubuskie' },
      { value: 'Łódzkie', viewValue: 'Łódzkie' },
      { value: 'Małopolskie', viewValue: 'Małopolskie' },
      { value: 'Mazowieckie', viewValue: 'Mazowieckie' },
      { value: 'Opolskie', viewValue: 'Opolskie' },
      { value: 'Podkarpackie', viewValue: 'Podkarpackie' },
      { value: 'Podlaskie', viewValue: 'Podlaskie' },
      { value: 'Pomorskie', viewValue: 'Pomorskie' },
      { value: 'Śląskie', viewValue: 'Śląskie' },
      { value: 'Świętokrzyskie', viewValue: 'Świętokrzyskie' },
      { value: 'Warmińsko-mazurskie', viewValue: 'Warmińsko-mazurskie' },
      { value: 'Wielkopolskie', viewValue: 'Wielkopolskie' },
      { value: 'Zachodniopomorskie', viewValue: 'Zachodniopomorskie' },
    ];

    public kolory = [
      { value: 'Wszystkie', viewValue: 'Wszystkie' },
      { value: 'Biały', viewValue: 'Biały' },
      { value: 'Szary', viewValue: 'Szary' },
      { value: 'Czarny', viewValue: 'Czarny' },
      { value: 'Czerwony', viewValue: 'Czerwony' },
      { value: 'Źółty', viewValue: 'Źółty' },
      { value: 'Zielony', viewValue: 'Zielony' },
      { value: 'Niebieski', viewValue: 'Niebieski' },
      { value: 'Fioletowy', viewValue: 'Fioletowy' },
      { value: 'Pomarańczowy', viewValue: 'Pomarańczowy' },
      { value: 'Brązowy', viewValue: 'Brązowy' },
      { value: 'Różowy', viewValue: 'Różowy' },
      { value: 'Żółty', viewValue: 'Żółty' },
      { value: 'Srebrny', viewValue: 'Srebrny' },
      { value: 'Seledynowy', viewValue: 'Seledynowy' },
    ];

    newGuid() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        // tslint:disable-next-line:no-bitwise
        const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }

}
