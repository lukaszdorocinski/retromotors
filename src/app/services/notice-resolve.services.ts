import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Notice } from '../GlobalDataModel/notice';
import { NoticeService } from './notice.service';

@Injectable()
export class NoticeResolve implements Resolve<Notice> {
    constructor(private noticeService: NoticeService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.noticeService.getNoticeByGuid(route.params['guid']);
    }
}
