import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, Jsonp, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Select } from '../GlobalDataModel/Select';
import { Promise } from 'q';
import { User } from '../GlobalDataModel/user.model';
import { GlobalService } from './global.service';

// tslint:disable:max-line-length

@Injectable()
export class UserService {
    public token: string;
    data: any[];
    user: User;

    private _getUser = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/select';
    private _addUser = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/registry';
    private _updateUser = 'http://api.retromotors.pl/7VTbq7uPf2p5Y8R6/update';

    constructor(
        private http: Http,
        private globalService: GlobalService,
    ) {
        // const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // this.token = currentUser && currentUser.token;
    }

    getUser(select: any): Observable<any> {
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map(response => {
                if (response.status < 200 || response.status >= 300) {
                    return false;
                } else {
                    this.data = response.json();
                    if (this.data.length === 0) {
                        return false;
                    } else {
                        // console.log(this.data[0]);
                        localStorage.setItem('currentUser', JSON.stringify({ username: this.data[0].email }));
                        // window.localStorage.setItem('currentUser', this.data[0].email);
                        return true;
                    }
                }
            });
    }

    updateUser(select: any): Observable<any> {
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.update, body, options)
            .map(response => {
                if (response.status < 200 || response.status >= 300) {
                    return false;
                } else {
                    this.data = response.json();
                    if (this.data.length === 0) {
                        return false;
                    } else {
                        return true;
                    }
                }
            });
    }

    getUserById(id: number): Observable<User> {
        const select = { table: 'users', bind: ['id', 'email', 'password', 'salt', 'hash_alg', 'registry_date', 'last_login', 'nick_name', 'session_hash'], where: { id: id } };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <any>response.json());
    }

    getUserByToken(token: string): Observable<User> {
        const select = { table: 'users', bind: ['id', 'email', 'password', 'salt', 'hash_alg', 'registry_date', 'last_login', 'nick_name', 'session_hash'], where: { session_hash: token } };
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.select, body, options)
            .map((response: Response) => <any>response.json());
    }

    addUser(select: any) {
        const body = JSON.stringify(select);
        const username = this.globalService.username;
        const password = this.globalService.password;
        const headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.globalService.registry, body, options)
            .map((response: Response) => <any>response.json());
    }

    // przygotowawszy
    private extractData(res: Response) {
        const body = res.json();
        return body.data || {};
    }
    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
}
