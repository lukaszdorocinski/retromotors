
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { Headers, Http} from '@angular/http';
import {JsonpModule, Jsonp, Response} from '@angular/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './GlobalDataModel/auth.guard';

// pluginy
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';  // upload grafiki

// material
import {MatFormFieldModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatGridListModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatDatepickerModule, MAT_DATE_LOCALE, MatNativeDateModule, DateAdapter} from '@angular/material';
import { HttpModule } from '@angular/http';
import {MatCheckboxModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatTabsModule} from '@angular/material/tabs';
import {MatStepperModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';


import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { SearchComponent } from './search/search.component';
import { UserService } from './services/user.service';
import { SharedService } from './services/shared.service';
import { GlobalService } from './services/global.service';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterService } from './services/register.service';
import { PromotedComponent } from './promoted/promoted.component';
import { TermsComponent } from './terms/terms.component';
import { UserPanelComponent } from './user-panel/user-panel.component';
import { NoticeComponent } from './notice/notice.component';
import { NoticeListComponent } from './notice-list/notice-list.component';
import { UserResolve } from './services/user-resolve.services';
import { NoticeResolve } from './services/notice-resolve.services';
import { NoticeService } from './services/notice.service';
import { AuthenticationService } from './services/authentication.service';
import { MailService } from './services/mail.service';
import { ConfirmationComponent } from './confirmation/confirmation.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { UserDetailsComponent } from './user-details/user-details.component';
import { FooterComponent } from './footer/footer.component';
import { NoticeAddComponent } from './notice-add/notice-add.component';
import { NoticeEditComponent } from './notice-edit/notice-edit.component';
import { NoticeItemComponent } from './notice-item/notice-item.component';

import { NgxGalleryModule } from 'ngx-gallery';
import { CookieService } from 'ngx-cookie-service';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { ContactComponent } from './contact/contact.component';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { AssistanceComponent } from './assistance/assistance.component';
import { NgcCookieConsentModule, NgcCookieConsentConfig } from 'ngx-cookieconsent';

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'retromotors.pl'
  },
  palette: {
    popup: {
      background: '#000'
    },
    button: {
      background: '#f1d600'
    }
  },
  theme: 'edgeless',
  type: 'opt-out',
  content: {
    message: 'Serwis wykorzystuje pliki cookes. Korzystając wyrażasz zgodę na wykorzystywanie plików cookies.',
    dismiss: 'Rozumiem!',
    deny: '',
    link: 'Dowiedz się więcej',
    href: 'http://retromotors.pl/Polityka-Cookie'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    SearchComponent,
    LoginComponent,
    RegisterComponent,
    PromotedComponent,
    TermsComponent,
    UserPanelComponent,
    NoticeComponent,
    NoticeListComponent,
    UserDetailsComponent,
    FooterComponent,
    NoticeAddComponent,
    NoticeEditComponent,
    NoticeItemComponent,
    ConfirmationComponent,
    ContactComponent,
    CookiePolicyComponent,
    AdvertisementComponent,
    AssistanceComponent,
  ],
  imports: [
    AppRoutingModule,
    HttpModule,
    JsonpModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatSelectModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatMenuModule,
    MatListModule,
    MatTabsModule,
    MatStepperModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    CommonModule,
    FancyImageUploaderModule,
    NgxGalleryModule,
    BrowserAnimationsModule,
    BrowserModule,
    RecaptchaFormsModule,
    RecaptchaModule.forRoot(),
    NgcCookieConsentModule.forRoot(cookieConfig),
  ],
  providers: [
    UserService,
    SharedService,
    GlobalService,
    RegisterService,
    NoticeService,
    UserResolve,
    NoticeResolve,
    AuthenticationService,
    MailService,
    AuthGuard,
    CookieService,
    // {provide: DateAdapter, useClass: MyDateAdapter},
    {provide: MAT_DATE_LOCALE, useValue: 'pl-PL'},
    { provide: RECAPTCHA_SETTINGS, useValue: { siteKey: '6LeJdVMUAAAAAPvmrFU9XsGqi9YWThIRtc_x_cUR' } as RecaptchaSettings, },
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
