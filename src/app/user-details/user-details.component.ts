import { Component, OnInit } from '@angular/core';
import { User } from '../GlobalDataModel/user.model';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  modelForm: FormGroup;
  modelFormPass: FormGroup;
  username = '';
  disabledInput = true;

  hide = true; // do wpisywania hasła
  validationParametr = false; // parametr for check validation fields

  formErrors = {
    oldPass: '',
    newPass: '',
    newPassRet: '',
  };

  private validationMessages = {
    oldPass: {
      required: 'Podanie aktualnego hasła jest wymagane'
    },
    newPass: {
      required: 'Podanie nowego hasła jest wymagane'
    },
    newPassRet: {
      required: 'Podanie nowego hasła jest wymagane'
    },
  };

  constructor(
    private formBuldier: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private authService: AuthenticationService,
    private cookieService: CookieService,
  ) { }

  ngOnInit() {
    if (!this.authService.isLoggedIn) {
      this.authService.setLoalStorage();
    }
    const userData = JSON.parse(localStorage.getItem('currentUser'));
    if (userData.nickname !== null) {
      this.username = userData.nickname;
    } else { this.username = userData.username; }

    // this.user.email = userData.username;
    // this.user.nick_name = userData.nickname;

    this.initModelFormUserPass();

    this.modelFormPass.valueChanges.subscribe((value: FormGroup) => {
      this.onControlValueChanged();
    });
    this.onControlValueChanged();

    this.modelForm = new FormGroup({
      'email': new FormControl(userData.username),
      'nick_name': new FormControl(userData.nickname),
    });
  }

  initModelFormUserPass() {
    this.modelFormPass = this.formBuldier.group({
      oldPass: ['', Validators.required],
      newPass: ['', Validators.required],
      newPassRet: '',
    }, {
        validator: this.comparePassword.bind(this)
      });
  }

  onControlValueChanged(): void {
    const form = this.modelFormPass;
    // tslint:disable-next-line:forin
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (this.validationParametr === false) {
        if (control && control.dirty && !control.valid) {
          const validationMessages = this.validationMessages[field];
          // tslint:disable-next-line:forin
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      } else {
        if (control) {
          const validationMessages = this.validationMessages[field];
          // tslint:disable-next-line:forin
          for (const key in control.errors) {
            this.formErrors[field] += validationMessages[key] + ' ';
          }
        }
      }
    }
  }

  checkValidation(): void {
    this.validationParametr = true;
    const form = this.modelFormPass;
    // tslint:disable-next-line:forin
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control) {
        const validationMessages = this.validationMessages[field];
        // tslint:disable-next-line:forin
        for (const key in control.errors) {
          this.formErrors[field] += validationMessages[key] + ' ';
        }
      }
    }
  }

  comparePassword(group: FormGroup) {
    if (group.value.newPass === group.value.newPassRet) {
      return null;
    } else {
      return {
        invalidComparePassword: true
      };
    }
  }

  onSubmit(form: FormGroup) {
    const nick_name = form.get('nick_name').value;
    const email = form.get('email').value;
    // tslint:disable-next-line:max-line-length
    const select = { table: 'users', bind: { nick_name: nick_name, email: email }, where: { session_hash: this.cookieService.get('token') } };
    this.userService.updateUser(select)
      .subscribe(
      data => {
        if (data === true) {
          this.username = nick_name;
          localStorage.setItem('currentUser', JSON.stringify({ username: email, nickname: nick_name }));
        } else {
          // console.log('błąd');
        }
      });
  }

  onSubmitPass(form: FormGroup) {
    const oldPass = form.get('oldPass').value;
    const newPass = form.get('newPass').value;
    const newPassRet = form.get('newPassRet').value;
    const token = this.cookieService.get('token');
    const select = { table: 'users', bind: ['password'], where: {session_hash: token } };
  }
}
