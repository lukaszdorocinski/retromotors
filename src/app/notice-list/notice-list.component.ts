import { Component, OnInit, Input, SimpleChange, OnChanges } from '@angular/core';
import { Notice } from '../GlobalDataModel/notice';
import { Search } from '../GlobalDataModel/search';
import { ActivatedRoute } from '@angular/router/src/router_state';
import { NoticeService } from '../services/notice.service';
import { templateJitUrl } from '@angular/compiler';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-notice-list',
  templateUrl: './notice-list.component.html',
  styleUrls: ['./notice-list.component.css']
})
export class NoticeListComponent implements OnChanges {
  @Input() data: Notice[];

  private allItems: Notice[];
  pager: any = {};
  pagedItems: any[];

  constructor(
    private noticeService: NoticeService,
    private router: Router,
  ) { }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (this.data) {
      if (this.data.length > 0) {
        this.setPage(1);
      }
    }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.noticeService.getPager(this.data.length, page);

    this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
